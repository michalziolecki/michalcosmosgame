/* Projekt Micha� Zi�ecki*/

//biblioteki natywne
#include <iostream>
#include <cstdlib>
#include <ctime>
//biblioteki zainstalowane
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

using namespace std;

class KlasaMeteoryt
{
private:
    double pozycja_X = 1300;
    double pozycja_Y = 340;
    double predkosc;
    SDL_Rect RectMeteoryt;
    SDL_Renderer *Ekran;
    SDL_Texture *Rys_Meteoryt;

public:

    KlasaMeteoryt(SDL_Renderer *Render, SDL_Texture *Textura, double ruch)
    {
        Ekran = Render;
        Rys_Meteoryt = Textura;
        predkosc = ruch;
    }

    double getPozycja_X()
    {
        return pozycja_X;
    }

    double getPozycja_Y()
    {
        return pozycja_Y;
    }

    void losujMeteoryty()
    {
        //if (klatka%100 == 0)
        pozycja_Y = 50 + rand()%550;
    }

    void ruchMeteorytu()
    {
        pozycja_X = pozycja_X - predkosc;
    }

    void wyswietlMeteoryt() // tworzenie z sdl powierzchni wyswietlajacej grafike meteorytu
    {
            RectMeteoryt.x = pozycja_X;
            RectMeteoryt.y = pozycja_Y;
            RectMeteoryt.h = 40;
            RectMeteoryt.w = 40;
            SDL_RenderCopy(Ekran, Rys_Meteoryt, NULL,&RectMeteoryt);
            //SDL_RenderCopyEx(Ekran, Rys_Meteoryt, NULL,&RectMeteoryt, 0 , NULL, SDL_FLIP_VERTICAL);
            //przycisk[SDL_SCANCODE_CLEAR];
    }
};

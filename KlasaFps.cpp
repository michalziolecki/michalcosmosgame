/* Projekt Michal Ziolecki*/

//biblioteki natywne
#include <iostream>
//biblioteki zainstalowane
#include <SDL2/SDL.h>

using namespace std;

class KlasaFps
{
private:
    int start = 0;
    int koniec = 0;
    int roznica = 0;
    int klatka = 0;
    float fps;
    float opoznienie;
public:
    KlasaFps (float ile)
    {
        fps = ile;
    }

    void setStart()
    {
        start = SDL_GetTicks();
    }

    void setKoniec()
    {
        koniec = SDL_GetTicks();
    }

    int getKlatka()
    {
        return klatka;
    }

    int opoznij()
    {
        roznica =  koniec - start;
        opoznienie = (1000/fps)-roznica; // ustalam ilosc milisekund opoznienia po kazdej petli: (1000/fps) ile petli w sekunde minus czas trwania petli

        if(opoznienie > 0)
        {
            SDL_Delay(opoznienie);
        }
        klatka ++;
        if(klatka>=193) klatka=0; //zmiane polozenia robi gdy skrajne obrazki sie koncza i zaczynaja
        return klatka;
    }

};

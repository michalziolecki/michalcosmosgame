/* Projekt Micha³ Zió³ecki*/

//biblioteki natywne
#include <iostream>
//biblioteki zainstalowane
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

using namespace std;

class KlasaPocisk
{
private:
    double pozycja_X;
    double pozycja_Y;
    double predkosc;
    SDL_Rect RectPocisk;
    SDL_Renderer *Ekran;
    SDL_Texture *Rys_Pocisk;

public:

    KlasaPocisk(SDL_Renderer *Render, SDL_Texture *Tekstura, double poz_X, double poz_Y, double speed)
    {
        Ekran = Render;
        Rys_Pocisk = Tekstura;
        pozycja_X = poz_X+80;
        pozycja_Y = poz_Y+10;
        predkosc = speed;
    }

    double getPozycja_X()
    {
        return pozycja_X;
    }
    double getPozycja_Y()
    {
        return pozycja_Y;
    }

    void wyswietlPocisk()
    {
        pozycja_X = pozycja_X + predkosc;
        pozycja_Y = pozycja_Y;
        RectPocisk.h = 20;
        RectPocisk.w = 20;
        RectPocisk.x = pozycja_X;
        RectPocisk.y = pozycja_Y;
        //cout<< "Proba wyswieltnania pocisku ... "
        SDL_RenderCopy(Ekran, Rys_Pocisk, NULL, &RectPocisk);
    }
};

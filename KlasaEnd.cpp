/* Projekt Micha³ Zió³ecki*/

//biblioteki natywne
#include <iostream>
//biblioteki zainstalowane
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

using namespace std;

class KlasaEnd
{
private:
    SDL_Rect Rect_GameOver;
    SDL_Rect Rect_Boom;
    SDL_Renderer * Ekran;
    SDL_Event Zdarzenie;
    SDL_Texture *BackGround;
    SDL_Texture *Boom;

public:

    KlasaEnd(SDL_Renderer *Render, SDL_Texture *Texture, SDL_Texture *Texture_Boom, SDL_Event Event)
    {
        Ekran = Render;
        BackGround = Texture;
        Boom = Texture_Boom;
        Zdarzenie = Event;
    }

    void ZamkniecieOkna()
    {
        if(SDL_PollEvent(&Zdarzenie)) // zamykanie okna krzyzykiem, usuwana zdarzenie ze stosu
        {
            if(Zdarzenie.type == SDL_QUIT) // wywolanie zakmniecia okna
            {
                exit(0); //zakmniecie okna
            }
        }
    }

    void wyswietlBoom(double pozycjaPoj_X, double pozycjaPoj_Y)
    {
        Rect_Boom.x = pozycjaPoj_X + 10; // przekazać pozycje pojazdu
        Rect_Boom.y = pozycjaPoj_Y - 25;
        Rect_Boom.h = 100;
        Rect_Boom.w = 100;
        SDL_RenderCopy(Ekran, Boom, NULL, &Rect_Boom);
        SDL_RenderPresent(Ekran);
        //cout<< "wchodze tu - gwiazdka" <<cout;
        SDL_Delay(1000);

    }

    void wyswietlGameOver()
    {
            Rect_GameOver.x = 0;
            Rect_GameOver.y = 0;
            Rect_GameOver.h = 680;
            Rect_GameOver.w = 1280;
            SDL_RenderCopy(Ekran, BackGround, NULL, &Rect_GameOver); // tworzenie tekstury w okreslonym miejscu i rozmiarze
            SDL_RenderPresent(Ekran);
            SDL_Delay(3000);

    }
};

/* Projekt Micha� Zi�ecki*/

//biblioteki natywne
#include <iostream>
//biblioteki zainstalowane
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

using namespace std;

class KlasaTlo
{
private:
    SDL_Rect Rect_tlo;
    SDL_Renderer * Ekran;
    SDL_Event Zdarzenie;
    SDL_Texture *Tlo;
    int klatka;

public:

    KlasaTlo(SDL_Renderer *Render, SDL_Texture *Texture, SDL_Event Event)
    {
        Ekran = Render;
        Tlo = Texture;
        Zdarzenie = Event;
    }

    void zamknijOkno()
    {
        if(SDL_PollEvent(&Zdarzenie)) // zamykanie okna krzyzykiem, usuwana zdarzenie ze stosu
        {
            if(Zdarzenie.type == SDL_QUIT) // wywolanie zakmniecia okna
            {
                exit(0); //zakmniecie okna
            }
        }
    }

    void wyswietlTlo(int klatka)
    {
        for(int i = 0; i < 8; i++)
        {
            Rect_tlo.x = i*193-(klatka*1);//%193; //i rozklada rownomiernie ekran a klatka to przesuwa i % zmiane polozenia robi gdy skrajne obrazki sie koncza i zaczynaja
            Rect_tlo.y = 0;
            Rect_tlo.h = 720;
            Rect_tlo.w = 193;
            //cout<<"robimy tlo"<<endl;
            SDL_RenderCopy(Ekran,Tlo,NULL,&Rect_tlo); // tworzenie tekstury w okreslonym miejscu i rozmiarze
        }
    }
};

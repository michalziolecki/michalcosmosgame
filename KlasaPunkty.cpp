/* Projekt Michal Zi�lecki*/

//biblioteki natywne
#include <iostream>
#include <string>
#include <sstream>

//biblioteki zainstalowane
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

using namespace std;

class KlasaPunkty
{
private:
    SDL_Renderer *Ekran;
    SDL_Rect RectPunktacja;
    int punktacja;

public:
    KlasaPunkty (SDL_Renderer *Render, int point)
    {
        Ekran = Render;
        punktacja = point;
    }

    void setPunktacja()
    {
        punktacja = punktacja + 1;
    }

    void wyswietlPunktacje (int points)
    {
        TTF_Init();
        SDL_Surface *surface_tekst;
        SDL_Texture *texture_tekst;
        TTF_Font *czcionka = TTF_OpenFont("arial.ttf", 30 );
        SDL_Color kolor = {255,255,255};
        stringstream ss;
        //string wynik;
        ss << points;
        //wynik = ss.str(); //ss.str().c_str()
        RectPunktacja.x = 0;
        RectPunktacja.y = 0;
        RectPunktacja.w = 50;
        RectPunktacja.h = 50;


        surface_tekst = TTF_RenderText_Blended(czcionka, ss.str().c_str(), kolor);  // tworze powierzchnie
        texture_tekst = SDL_CreateTextureFromSurface(Ekran, surface_tekst); // tworze teksture
        //cout<< punktacja<< endl;

        SDL_RenderCopy(Ekran, texture_tekst, NULL, &RectPunktacja); // wyswietlan ostatecznie teksture
        SDL_DestroyTexture(texture_tekst); // zwalnia teksture bo po zmienie ilosci punktow trzeba zrobic nowa
        SDL_FreeSurface(surface_tekst); // zwalaniam powierzchnie bo po zmianie punktacji robie nowa

    }
};

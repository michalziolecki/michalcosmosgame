/* Projekt Micha� Zi�ecki*/

//biblioteki natywne
#include <iostream>
#include <vector>
//biblioteki zainstalowane
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
//pliki klas
#include "KlasaPocisk.cpp"

vector <KlasaPocisk> tablica_pociskow;

using namespace std;

class KlasaBohater
{
private:
    double ruch_Y = 0;
    double ruch_X = 0;
    double pozycja_X = 50;
    double pozycja_Y = 340;
    float katObrotu = 1;
    int licznik_spacji = 0;
    SDL_Rect RectBohater;
    SDL_Renderer *Ekran;
    SDL_Texture *Rys_Bohater;
    SDL_Texture *Rys_Pocisk;
    const Uint8 *przycisk = SDL_GetKeyboardState(NULL);

public:
    double wspolrzedna_X = -100;
    double wspolrzedna_Y = -100;

    KlasaBohater(SDL_Renderer *Render, SDL_Texture *Textura1, SDL_Texture *Textura2 )
    {
        Ekran = Render;
        Rys_Bohater = Textura1;
        Rys_Pocisk = Textura2;
    }

    double getPozycja_X()
    {
        return pozycja_X;
    }

    double getPozycja_Y()
    {
        return pozycja_Y;
    }

    void sterujBohaterem()
    {

        // pobieranie ruchu
        if(przycisk[SDL_SCANCODE_DOWN])
        {
            ruch_Y =+ 0.7;
        }
        else if(przycisk[SDL_SCANCODE_UP])
        {
            ruch_Y =- 0.7;
        }
        if(przycisk[SDL_SCANCODE_LEFT])
        {
            ruch_X =- 2.0;
        }
        else if(przycisk[SDL_SCANCODE_RIGHT])
        {
            ruch_X =+ 1.0;
        }
        else if(przycisk[SDL_SCANCODE_SPACE])
        {
            licznik_spacji++;

            if(licznik_spacji%20==0)
            {
                KlasaPocisk Pocisk(Ekran, Rys_Pocisk, pozycja_X, pozycja_Y, 2.5); // tu moze by blad z pozycja bo przed zmiana
                Pocisk.wyswietlPocisk();
                tablica_pociskow.push_back(Pocisk);
            }
        }

        //fizyka ruchu
        //---------PION-------------
        pozycja_Y = pozycja_Y + ruch_Y; // zmiana polozenia w pionie
        if(pozycja_Y < 50) // gorna granica ruchu pojazdem
        {
            pozycja_Y = 50;
        }
        else if(pozycja_Y > 600) // dolna ganica ruchu
        {
            pozycja_Y = 600;
        }
        katObrotu = ruch_Y;
        ruch_Y = 0; // zerowanie ruchu do nastepnego klikniecia
        //---------POZIOM-------------
        pozycja_X = pozycja_X + ruch_X; // zmiana polozenia w poziomie
        if(pozycja_X < 30) // lewa granica ruchu pojazdem
        {
            pozycja_X = 30;
        }
        else if(pozycja_X > 1100) // prawa ganica ruchu
        {
            pozycja_X = 1100;
        }
        ruch_X = 0; // zerowanie ruchu do nastepnego klikniecia

    }

    void prowadzPocisk()
    {
        for (int i = 0; i < tablica_pociskow.size(); i++ ) // prowadzenie i wyswietlanie pocisku
        {
            tablica_pociskow.at(i).wyswietlPocisk();
        }
    }

    void wyczyscPocisk()
    {
        //cout<<"Wchodze do metody wyczyc pocisk"<<endl;
        for (int i = 0; i < tablica_pociskow.size(); i++ ) // usuwanie pocisku za ekranem i zwalnianie tablicy
        {
            //cout<<"Przeszukuje tablice"<<endl;
            if(tablica_pociskow.at(i).getPozycja_X() > 1300)
            {
                //cout<< "zabieram sie za kasowanie"<<endl;
                tablica_pociskow.erase(tablica_pociskow.begin() + i);
                break;
            }
        }
    }

    //przekazywanie informacji o wpolrzednych pocisku
    int ilePociskow()
    {
        return tablica_pociskow.size();
    }

    double wspolrzednaPocisku_X(int indeks)
    {
        wspolrzedna_X = tablica_pociskow.at(indeks).getPozycja_X();
        return wspolrzedna_X;

    }

    double wspolrzednaPocisku_Y(int indeks)
    {
        wspolrzedna_Y = tablica_pociskow.at(indeks).getPozycja_Y();
        return wspolrzedna_Y;
    }
    void usunPocisk(int indeks)
    {
        tablica_pociskow.erase(tablica_pociskow.begin() + indeks);
        //cout << "usunieto pocisk" << endl;
    }
    //koniec przekazywania

    void wyswietlBohatera() // tworzenie z sdl powierzchni wyswietlajacej grafike bohatera
    {
        RectBohater.x = pozycja_X;
        RectBohater.y = pozycja_Y;
        RectBohater.h = 40;
        RectBohater.w = 80;
        // SDL_RenderCopy(Ekran, Rys_Bohater, NULL, &RectBohater);
        SDL_RenderCopyEx(Ekran, Rys_Bohater, NULL,&RectBohater, katObrotu*25, NULL, SDL_FLIP_VERTICAL);
        //przycisk[SDL_SCANCODE_CLEAR];
    }

};

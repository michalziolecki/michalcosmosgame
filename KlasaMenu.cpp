/* Projekt Michal Ziolecki*/

//biblioteki natywne
#include <iostream>
//biblioteki zainstalowane
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

using namespace std;

class KlasaMenu
{
private:
    SDL_Rect Rect_Menu;
    SDL_Rect Rect_Opcje;
    SDL_Rect Rect_Strzalka;
    SDL_Renderer *Ekran;
    SDL_Event Zdarzenie;
    SDL_Texture *Rys_Menu;
    SDL_Texture *Rys_Opcje;
    SDL_Texture *Rys_Strzalka;
    double pozycja_X = 400.0;
    double pozycja_Y = 305.0;
    const Uint8 *przycisk = SDL_GetKeyboardState(NULL); //ew uint8
    bool rozpocznijGre = false;
    bool cofnijDoMenu = false;


public:

    KlasaMenu(SDL_Renderer *Render, SDL_Texture *Texture_Menu, SDL_Texture *Texture_Sterowanie,
              SDL_Texture *Texture_Strzalka, SDL_Event Event)
    {
        Ekran = Render;
        Rys_Menu = Texture_Menu;
        Rys_Opcje = Texture_Sterowanie;
        Rys_Strzalka = Texture_Strzalka;
        Zdarzenie = Event;
    }

    void sterujStrzalka()
    {
        if(przycisk[SDL_SCANCODE_DOWN] && pozycja_Y == 305)
        {
            pozycja_Y = 440;
        }
        if(przycisk[SDL_SCANCODE_DOWN] && pozycja_Y == 440); //nic;

        if(przycisk[SDL_SCANCODE_UP] && pozycja_Y == 440)
        {
            pozycja_Y = 305;
        }
        if(przycisk[SDL_SCANCODE_UP] && pozycja_Y == 305); //nic;

        if(pozycja_Y == 440 && (przycisk[SDL_SCANCODE_KP_ENTER] || przycisk[SDL_SCANCODE_SPACE]) )
        {
            while(cofnijDoMenu != true)
            {
                if(przycisk[SDL_SCANCODE_ESCAPE])
                {
                    cofnijDoMenu = true;
                }
                ZamkniecieOkna(); //zamykanie okna krzyzykiem
                SDL_RenderClear(Ekran);  //wyczyszczenie ekranu przed zaladowaniem
                wyswietlOpcje();
                SDL_RenderPresent(Ekran); // reprezentacja obrazu na monitor
            }
            cofnijDoMenu = false;
        }

        if(pozycja_Y == 305 && (przycisk[SDL_SCANCODE_KP_ENTER] || przycisk[SDL_SCANCODE_SPACE]) )
        {
            rozpocznijGre = true;
        }
    }

    int getGraj()
    {
        return rozpocznijGre;
    }

    void ZamkniecieOkna()
    {
        if(SDL_PollEvent(&Zdarzenie)) // zamykanie okna krzyzykiem, usuwana zdarzenie ze stosu
        {
            if(Zdarzenie.type == SDL_QUIT) // wywolanie zakmniecia okna
            {
                exit(0); //zakmniecie okna
            }
        }
    }

    void wyswietlMenu()
    {
        Rect_Menu.x = 0; // przekazaŠ pozycje pojazdu
        Rect_Menu.y = 0;
        Rect_Menu.h = 680;
        Rect_Menu.w = 1280;
        SDL_RenderCopy(Ekran, Rys_Menu, NULL, &Rect_Menu);
    }

    void wyswietlOpcje()
    {
        //cout<<"wchodze" <<endl;
        Rect_Opcje.x = 0; // przekazaŠ pozycje pojazdu
        Rect_Opcje.y = 0;
        Rect_Opcje.h = 680;
        Rect_Opcje.w = 1280;
        SDL_RenderCopy(Ekran, Rys_Opcje, NULL, &Rect_Opcje);
    }

    void wyswietlStrzalka()
    {

        Rect_Strzalka.x = pozycja_X; // przekazaŠ pozycje pojazdu
        Rect_Strzalka.y = pozycja_Y;
        Rect_Strzalka.h = 50;
        Rect_Strzalka.w = 50;
        SDL_RenderCopy(Ekran, Rys_Strzalka, NULL, &Rect_Strzalka);

    }
};

/* Projekt Micha� Zi�ecki*/
//biblioteki natywne
#include <iostream>
#include <vector>
//biblioteki zainstalowane
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
//pliki klas
#include "KlasaFps.cpp"
#include "KlasaTlo.cpp"
#include "KlasaBohater.cpp"
#include "KlasaMeteoryt.cpp"
#include "KlasaPunkty.cpp"
#include "KlasaEnd.cpp"
#include "KlasaMenu.cpp"
//#include "KlasaPocisk.cpp"

using namespace std;

//obiekty tworzenia okna
SDL_Window * Okno;
SDL_Renderer * Ekran;
SDL_Event Zdarzenie;
SDL_Rect Rect1, Rect2;


//funckja zamieniajaca powierzchnie w teksture, zeby uzywac karty graficznej
SDL_Texture *zaladujTeksture(std::string path)
{
    SDL_Texture* NowaTekstura = NULL;
    SDL_Surface* ZaladowanaTekstura = IMG_Load(path.c_str()); // ladowanie powierzchni ze sciezki
    if(ZaladowanaTekstura == NULL)
    {
        cout<<"Nie mozna zaladowac obrazu"<< path.c_str() << IMG_GetError();
    }
    else
    {
        NowaTekstura =  SDL_CreateTextureFromSurface(Ekran, ZaladowanaTekstura); // zamiana powierzhcni w teksture

        if(NowaTekstura == NULL)
        {
            cout<< "Nie mozna stworzyc tekstury z powierzchni za pomoca metody SDL_CreateTextureFromSurface" << IMG_GetError();
        }

        SDL_FreeSurface(ZaladowanaTekstura); // jesli nie udalo sie stworzyc nowej tekstury usuwa
    }

    return NowaTekstura;
}

int main(int argc, char *argv[])
{
    // Zadalodwanie grafiki
    Okno = SDL_CreateWindow("Cosmos Game by Michal Ziolecki BETA", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 680, 0); //utworzenie okna windows
    Ekran = SDL_CreateRenderer(Okno,-1, SDL_RENDERER_ACCELERATED); // renderowanie powierzhcni ekranu
    TTF_Init();
    SDL_Texture *Tlo = zaladujTeksture("tlo.png"); // zaladowanie obrazu jako tekstu przez odwolanie do funkcji zmieniajacej
    SDL_Texture *Rys_Bohater = zaladujTeksture("bohater.png"); // zaladowanie obrazu bohater
    SDL_Texture *Rys_Mateoryt = zaladujTeksture("meteoryt.png"); // zaladowanie obrazu meteoryt
    SDL_Texture *Rys_Pocisk = zaladujTeksture("pocisk.png");
    SDL_Texture *Rys_GameOver = zaladujTeksture("GameOver.png");
    SDL_Texture *Rys_Boom = zaladujTeksture("Boom.png");
    SDL_Texture *Rys_Menu = zaladujTeksture("Menu.png");
    SDL_Texture *Rys_Sterowanie = zaladujTeksture("Sterowanie.png");
    SDL_Texture *Rys_Strzalka = zaladujTeksture("Strzalka.png");

    //obiekty
    KlasaFps Fps(200);
    KlasaTlo Scenografia(Ekran, Tlo, Zdarzenie);
    KlasaBohater Bohater(Ekran, Rys_Bohater, Rys_Pocisk);
    KlasaPunkty Punkty(Ekran, 0);
    KlasaEnd Zakonczenie(Ekran, Rys_GameOver, Rys_Boom, Zdarzenie);
    KlasaMenu Menu (Ekran, Rys_Menu, Rys_Sterowanie, Rys_Strzalka, Zdarzenie);
    vector <KlasaMeteoryt> tablica_meteorytow; // tworze tablice meteorytow, w niej dynamicznie zapisuje obiekty do wyswietlania

    //zmienne
    int ilePociskow = 0;
    double wspolrzednaPocisku_X = -100;
    double wspolrzednaPocisku_Y = -100;
    bool wyjscie_z_petli = false;
    int points = 0;

    //MENU GRY
    while(true)
    {
        //---------START MENU-------------
        Fps.setStart();
        Menu.ZamkniecieOkna(); //zamykanie okna krzyzykiem
        SDL_RenderClear(Ekran);  //wyczyszczenie ekranu przed zaladowaniem

        Menu.wyswietlMenu();
        Menu.wyswietlStrzalka();
        Menu.sterujStrzalka();
        if(Menu.getGraj() == true) break;
        //Menu.wyswietlOpcje();

        //---------KONIEC MENU-------------
        Fps.setKoniec();
        Fps.opoznij();
        SDL_RenderPresent(Ekran); // reprezentacja obrazu na monitor

    }

    //GLOWNA PETLA GRY
    while(true)
    {
        //---------EKRAN-------------
        Fps.setStart();
        Scenografia.zamknijOkno(); //zamykanie okna krzyzykiem
        SDL_RenderClear(Ekran);  //wyczyszczenie ekranu przed zaladowaniem
        //---------BACKKGROUND-------------
        Scenografia.wyswietlTlo(Fps.getKlatka()); // wyswietlam w petli ruchome tlo zaleznie od fps i klatki

        //---------BOHATER-------------
        Bohater.sterujBohaterem();
        Bohater.wyswietlBohatera();
        Bohater.prowadzPocisk();
        //----------PUNKTY------------
        Punkty.wyswietlPunktacje(points); //test punktacji

        //---------METEORYT-------------
        // tworzenie obiektu meteoryt i przypisanie do tablicy dynamicznej
        if(Fps.getKlatka()%70 == 0)
        {
            KlasaMeteoryt Meteoryt(Ekran, Rys_Mateoryt, 2.2); // tworze obiekt
            Meteoryt.losujMeteoryty(); // losuje wyjsciowe polozenie obiektu
            tablica_meteorytow.push_back(Meteoryt); // dodaje do tablicy
        }


        //---------KONTROLA POZYCJI------------- POJAZD - METEORYT
        for(int i = 0; i < tablica_meteorytow.size(); i++)
        {
            //bool wyjscie_z_petli2 = false;
            if( tablica_meteorytow.at(i).getPozycja_X() > Bohater.getPozycja_X() && tablica_meteorytow.at(i).getPozycja_X() < Bohater.getPozycja_X() + 80
                    &&  tablica_meteorytow.at(i).getPozycja_Y() + 40 > Bohater.getPozycja_Y() && tablica_meteorytow.at(i).getPozycja_Y() < Bohater.getPozycja_Y() + 40 )
            {
                for(int i = 0; i < tablica_meteorytow.size(); i++) // czyszczenie pamieci przed zamknieciem gry
                {
                        tablica_meteorytow.erase(tablica_meteorytow.begin() + i);
                        break;
                }
                //ZAKONCZENIE GRY -- GME OVER
                //while(true) // wyswietlenie GAME OVER
                //{
                    Zakonczenie.wyswietlBoom(Bohater.getPozycja_X(), Bohater.getPozycja_Y());
                    Zakonczenie.wyswietlGameOver();
                    exit(0);
                //}

            }
            /*if(wyjscie_z_petli2 == true)
            {
                wyjscie_z_petli2 = false;
                break;
            }*/
        }


        // polozenie meteorytu, nadalnie ruchu i wyswietlenie
        for(int i = 0; i < tablica_meteorytow.size(); i++)
        {
            tablica_meteorytow.at(i).ruchMeteorytu();
            tablica_meteorytow.at(i).wyswietlMeteoryt();
            if (tablica_meteorytow.at(i).getPozycja_X() <= -50) // zwalnianie pamieci przez kasowanie obiektow metoda erase
            {
                tablica_meteorytow.erase(tablica_meteorytow.begin() + (i)); /*// poprzednio bylo (i-1) tlyko nie rozumiem czemu to zrobilem*/
                break;
            }
        }


        //---------KONTROLA POZYCJI POCISK- METEORYT-------------
        ilePociskow = Bohater.ilePociskow();

        for(int i = 0; i < tablica_meteorytow.size(); i++)
        {
            wyjscie_z_petli = false;
            for(int j = 0; j < ilePociskow; j++)
            {
                wspolrzednaPocisku_X = Bohater.wspolrzednaPocisku_X(j);
                wspolrzednaPocisku_Y = Bohater.wspolrzednaPocisku_Y(j);
                // warunki zbieznosci polozenia tekstur
                if( tablica_meteorytow.at(i).getPozycja_X() > wspolrzednaPocisku_X && tablica_meteorytow.at(i).getPozycja_X() < wspolrzednaPocisku_X + 20
                    &&  tablica_meteorytow.at(i).getPozycja_Y() + 40 > wspolrzednaPocisku_Y  && tablica_meteorytow.at(i).getPozycja_Y() < wspolrzednaPocisku_Y  + 20 )
                {
                            tablica_meteorytow.erase(tablica_meteorytow.begin() + i);
                            // cout<< "usunieto meteoryt" <<endl;
                            Bohater.usunPocisk(j);
                            wyjscie_z_petli = true; // zmienna do wyjscia z drugiej petli
                            points++;
                            //Punkty.setPunktacja(); // dodawanie punktacji
                            break; // wyjscie z pierwszej petli

                }
            }
            if (wyjscie_z_petli == true) // wyjscie z drugiej petli - chroni przed wchodzeniem w usunieta pamiec - bez tego petla kreci sie wiecej niz rozmiar po usunieciu
            {
                wyjscie_z_petli = false;
                break;
            }
        }
        //------KONIEC USUWANIA METEORYTOW PO TRAFIENIU---------

        Bohater.wyczyscPocisk();

        //---------EKRAN-------------
        Fps.setKoniec();
        Fps.opoznij();
        SDL_RenderPresent(Ekran); // reprezentacja obrazu na monitor
    }

    return 0;
}
